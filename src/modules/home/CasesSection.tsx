import * as React from 'react';
import useSWR from 'swr/dist';
import styled from '@emotion/styled';
import * as Sentry from '@sentry/browser';

import { Box, Heading, themeProps, Text, Stack } from 'components/design-system';
import { fetch } from 'utils/api';
// import { logEventClick } from 'utils/analytics';
import formatTime from 'utils/formatTime';
import DekontaminasiStatsResponse, {
  DEKONTAMINASI_STATS_API_URL,
  DEKONTAMINASI_STATS_TIMESTAMP_API_URL,
} from 'types/dekontaminasi';

const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

// const Link = Text.withComponent('a');

// const ReadmoreLink = styled(Link)`
//   text-decoration: none;

//   &:hover,
//   &:focus {
//     text-decoration: underline;
//   }
// `;

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.sm}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.md}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.lg}px / 2 - 48px), 1fr)
    );
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 4 - 48px), 1fr)
    );
  }
`;

export interface CaseBoxProps {
  color: string;
  value?: number;
  label: string;
}

const CaseBox: React.FC<CaseBoxProps> = React.memo(({ color, value, label }) => (
  <Box
    display="flex"
    alignItems="center"
    justifyContent="center"
    px="md"
    pt="sm"
    pb="md"
    borderRadius={6}
    backgroundColor="card"
  >
    <Box textAlign="center">
      <Text display="block" variant={1100} color={color} fontFamily="monospace">
        {value || EMPTY_DASH}
      </Text>
      <Text display="block" mb="xxs" variant={400} color="accents08">
        {label}
      </Text>
    </Box>
  </Box>
));

export interface CasesSectionBlockProps {
  data?: DekontaminasiStatsResponse;
  error: boolean;
  timestampData?: number;
  timestampError: boolean;
}

const CasesSectionBlock: React.FC<CasesSectionBlockProps> = React.memo(
  ({ data, error, timestampData, timestampError }) => {
    if (error) {
      Sentry.withScope(scope => {
        scope.setTag('api_error', 'case_summary');
        Sentry.captureException(error);
      });
    }

    const SectionHeading = styled(Heading)`
      margin-bottom: ${themeProps.space.md}px;

      ${themeProps.mediaQueries.md} {
        margin-bottom: ${themeProps.space.xl}px;
      }
    `;

    const DetailsWrapper = styled(Box)`
      ${themeProps.mediaQueries.md} {
        display: flex;
        align-items: flex-end;
        justify-content: space-between;
      }
    `;

    const lastUpdatedAt = !timestampError && timestampData;
    return (
      <Stack mb="xxl">
        <SectionHeading variant={800} as="h2">
          Jumlah Kasus di Indonesia Saat Ini
        </SectionHeading>
        <Box>
          <GridWrapper>
            <CaseBox color="chart" value={data?.numbers.infected} label="Terkonfirmasi" />
            <CaseBox
              color="warning02"
              value={data && data.numbers.infected - data.numbers.recovered - data.numbers.fatal}
              label="Dalam Perawatan"
            />
            <CaseBox color="success02" value={data?.numbers.recovered} label="Sembuh" />
            <CaseBox color="error02" value={data?.numbers.fatal} label="Meninggal" />
          </GridWrapper>
          <DetailsWrapper>
            <Box mt="md">
              <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
                {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
              </Text>
              <Text as="p" variant={400} color="accents07" fontFamily="monospace">
                {lastUpdatedAt ? formatTime(new Date(lastUpdatedAt), 'longest') : ERROR_MESSAGE}
              </Text>
            </Box>
            {/* TODO: Reenable later when AWS migration has been completed
          <Box mt="md">
            <ReadmoreLink
              href="http://kcov.id/statistik-harian"
              target="_blank"
              rel="noopener noreferrer"
              color="primary02"
              variant={500}
              fontWeight={600}
              onClick={() => logEventClick('Statistik Harian (cases section)')}
            >
              Lihat Statistik Harian &rarr;
            </ReadmoreLink>
          </Box> */}
          </DetailsWrapper>
        </Box>
      </Stack>
    );
  }
);

const CasesSection: React.FC = () => {
  const { data, error } = useSWR<DekontaminasiStatsResponse | undefined>(
    DEKONTAMINASI_STATS_API_URL,
    fetch
  );
  const { data: timestampData, error: timestampError } = useSWR<
    DekontaminasiStatsResponse | undefined
  >(DEKONTAMINASI_STATS_TIMESTAMP_API_URL, fetch);
  return (
    <CasesSectionBlock
      data={data}
      error={!!error}
      timestampData={Number(timestampData)}
      timestampError={!!timestampError}
    />
  );
};

export default CasesSection;
